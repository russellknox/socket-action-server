const setOnMessage = (response, actions) => async client => {
  try {
    const { type, payload } = JSON.parse(client)
    const action = actions[type]
    const clientResponse = await action(payload, response)
    clientResponse()
  } catch (error) {
    // deal with error
  }
}

export { setOnMessage }
