import WebSocket from 'ws'
import app from './server'

import { setOnMessage } from './events'
import { createResponses } from './response'

const createSocketServer = server => ({ verifyClient = null, port = 8080 }) => {
  // TODO: add custom configuration when creating server i.e. cors
  const config = {}

  const wss = new WebSocket.Server({
    server: server(config),
    verifyClient,
  })
  server.listen(port)
  return wss
}

const configSockets = wss => ({ plugins, actions: createActions, onInitialConnection }) => {
  wss.on('connection', (socket, req) => {
    // retur object with standard socket response {send, sendTo, sendToAll}
    const response = createResponses(socket, wss)
    const action = createActions(plugins, socket)

    const onMessage = setOnMessage(response, action)

    // set initial connection function
    if (onInitialConnection) {
      onInitialConnection(onMessage)
    }

    // listeners
    socket.on('message', onMessage)
    socket.on('error', () => 'ERROR')
  })
}

const socketServer = (server, sockets) => ({ serverOptions = {}, ...socketOptions }) => {
  const socket = server(serverOptions)
  sockets(socket)(socketOptions)
}

module.exports = socketServer(createSocketServer(app), configSockets)
