import bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express'
import http from 'http'

const createServer = () => {
  const app = express()
  app.use(cors())
  app.use(bodyParser.json({}))

  return http.createServer(app)
}

export default createServer
