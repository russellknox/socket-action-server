import{createResponses} from './response'

describe('responses', () => {
    test('send should send payload back to the request socket only', () => {
        const sendTo = jest.fn()
        const client = {
            send: p => sendTo(p)
        }
        const payload = {action: 'TEST'}
        createResponses(client, {}).send(payload)()
        expect(sendTo).toHaveBeenCalledTimes(1)
        expect(sendTo).toHaveBeenCalledWith(JSON.stringify(payload))
    })
    test('sendTo should send to the socket provided with the payload', () => {
        const sendTo = jest.fn()
        const client = {
            send: p => sendTo(p)
        }
        const payload = {action: 'TEST'}
        createResponses({}, {}).sendTo(client, payload)()
        expect(sendTo).toHaveBeenCalledTimes(1)
        expect(sendTo).toHaveBeenCalledWith(JSON.stringify(payload))
    })
    test('sendTo should send to the all socket provided with the payload', () => {
        const sendTo = jest.fn()
        const client = {
            send: p => sendTo(p)
        }
        const payload = {action: 'TEST'}
        createResponses({},{}).sendTo(client, payload)()
        expect(sendTo).toHaveBeenCalled()
        expect(sendTo).toHaveBeenCalledWith(JSON.stringify(payload))
    })
    test('sendToAll should send to the all sockets', () => {
        const sendTo = jest.fn()
        const client1 = {
            send: p => sendTo(p)
        }
        const client2 = {
            send: p => sendTo(p)
        }
        const payload = {action: 'TEST'}
        createResponses({}, {clients: [client1, client2]}).sendToAll(payload)()
        expect(sendTo).toHaveBeenCalledTimes(2)
        expect(sendTo).toHaveBeenCalledWith(JSON.stringify(payload))
    })
    test('sendToAll should send to the all sockets apart from the sending socket', () => {
        const sendTo = jest.fn()
        const client1 = {
            send: p => sendTo(p)
        }
        const client2 = {
            send: p => sendTo(p)
        }
        const payload = {action: 'TEST'}
        createResponses(client1, {clients: [client1, client2]}).sendToAll(payload)()
        expect(sendTo).toHaveBeenCalledTimes(1)
        expect(sendTo).toHaveBeenCalledWith(JSON.stringify(payload))
    })
})