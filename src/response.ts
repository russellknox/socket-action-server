const createResponses = (socket, wss) => {
  return {
    send: payload => () => {
      if (payload) {
        socket.send(JSON.stringify(payload))
      }
    },
    sendTo: (to, payload) => () => {
      if (Array.isArray(to)) {
        to.forEach(client => {
          client.send(JSON.stringify(payload))
        })
        return
      }
      to.send(JSON.stringify(payload))
    },
    sendToAll: payload => () => {
      wss.clients.forEach(client => {
        if (client !== socket) {
          client.send(JSON.stringify(payload))
        }
      })
    },
  }
}

export { createResponses }
