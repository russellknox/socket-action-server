import {setOnMessage} from './events'

describe('onMessage event', () => {
  const mockActions = (plugins, socket) => (payload, response) => null

  test('calls correct action based on type', async () => {
    const client = JSON.stringify({ type: 'ACTION' })
    const mockAction = jest.fn(mockActions)
    const action = {
      ACTION: mockAction,
    }
    setOnMessage({}, action)(client)
    expect(mockAction).toHaveBeenCalled()
  })
  test('action is passed payload and responses', async () => {
    const client = JSON.stringify({ type: 'ACTION', payload: 1234 })
    const mockAction = jest.fn(mockActions)
    const responses = { send: () => null }
    const action = {
      ACTION: (a, b) => mockAction(a, b),
    }
    setOnMessage(responses, action)(client)
    expect(mockAction).toHaveBeenCalledWith(JSON.parse(client).payload, responses)
  })
  test('throw error when ', async () => {
    expect(setOnMessage({}, {})({})).rejects.toBe('Error actionning incoming socket message')
  })
})
